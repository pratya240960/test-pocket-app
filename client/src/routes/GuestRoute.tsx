import React from 'react'
import { useSelector } from 'react-redux'
import { Navigate, Outlet } from 'react-router-dom'
import { RootState } from '../stores'

const GuestRoute: React.FC = () => {
  const profile = useSelector((state: RootState) => state.auths.profile)
  if (profile?.id) {
    return <Navigate to='/' />
  }
  return <Outlet />
}

export default GuestRoute
