import { configureStore } from '@reduxjs/toolkit'
import authReducer from './reducers/authReducer'

export const store = configureStore({
  reducer: {
    auths: authReducer
  }
})
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
