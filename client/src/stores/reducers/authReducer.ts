import { createSlice } from '@reduxjs/toolkit'
import { User } from '../../interfaces/userInterface'

export interface InitializeStateAuthenSlice {
  profile?: User
}
const initialState: InitializeStateAuthenSlice = {}
export const authSlice = createSlice({
  name: 'authen',
  initialState,
  reducers: {
    setProfile: (state, action) => {
      state.profile = action.payload
    }
  }
})
export const { setProfile } = authSlice.actions
export default authSlice.reducer
