import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Routes } from 'react-router-dom'
import DefaultLayout from './components/layouts/DefaultLayout'
import NotFoundPage from './components/pages/NotFoundPage'
import useLoadingWithProfile from './hooks/profileHook'
import HomePage from './pages/HomePage'
import LoginPage from './pages/LoginPage'
import RegisterPage from './pages/RegisterPage'
import AuthRoute from './routes/AuthRoute'
import GuestRoute from './routes/GuestRoute'
import { RootState } from './stores'

function App() {
  const { loading } = useLoadingWithProfile()
  const profile = useSelector((state: RootState) => state.auths.profile)
  if (loading) return <>Loading...</>
  return (
    <Routes>
      <Route path='/' element={<AuthRoute />}>
        <Route index element={<HomePage />} />
      </Route>
      <Route path='login' element={<GuestRoute />}>
        <Route index element={<LoginPage />} />
      </Route>
      <Route path='register' element={<GuestRoute />}>
        <Route index element={<RegisterPage />} />
      </Route>
      <Route path='*' element={<NotFoundPage />} />
    </Routes>
  )
}

export default App
