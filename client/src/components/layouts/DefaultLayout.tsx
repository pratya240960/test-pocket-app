import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../stores'
import { setProfile } from '../../stores/reducers/authReducer'
interface DefaultLayoutProps {
  children: JSX.Element | JSX.Element[] | string | string[]
}
const DefaultLayout: React.FC<DefaultLayoutProps> = ({ children }) => {
  const profile = useSelector((state: RootState) => state.auths.profile)
  const dispatch = useDispatch()
  const handleLogout = () => {
    dispatch(setProfile(undefined))
  }
  return (
    <>
      <header className=' flex justify-content-center' style={{ minHeight: '350px' }}>
        <div
          style={{
            backgroundImage: 'url("http://www2.sskru.ac.th/2021/wp-content/uploads/2021/10/DSC02805-scaled.jpg")',
            width: '100vw',
            minHeight: '350px',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
          }}
        >
          <div
            style={{ width: '100vw', minHeight: '350px', zIndex: 100, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
            className='top-0 py-5 flex justify-content-center'
          >
            <div style={{ maxWidth: '1280px', width: '100%' }} className='px-3'>
              <div style={{ maxWidth: '500px', width: '100%' }}>
                <img
                  src='http://www2.sskru.ac.th/2021/wp-content/uploads/2021/10/Untitled-3-e1640766293195.png'
                  className=' w-full'
                  alt=''
                />
              </div>
              <div className='page-title relative'>
                <div className=' bg-yellow-500 absolute bottom-0' style={{ width: '3px', height: '100%' }}></div>
                <h1 className='ml-5 font-bold text-white md:text-6xl uppercase mt-8'>ระบบยื่นคำร้อง petition system</h1>
              </div>
            </div>
          </div>
        </div>
      </header>
      <nav className='flex justify-content-center'>
        <div
          style={{ maxWidth: '1280px', width: '100%' }}
          className='flex justify-content-between align-items-center border-200 border-bottom-1 px-3'
        >
          <span className='text-gray-600 text-sm cursor-pointer' onClick={handleLogout}>
            ออกจากระบบ
          </span>
        </div>
      </nav>
      <main className=' flex justify-content-center'>
        <div style={{ maxWidth: '1280px', width: '100%' }} className='py-5 px-3'>
          {children}
        </div>
      </main>
    </>
  )
}

export default DefaultLayout
