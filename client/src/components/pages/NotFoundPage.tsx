import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../../stores'

const NotFoundPage: React.FC = () => {
  const profile = useSelector((state: RootState) => state.auths.profile)
  if (profile?.id) return <>404 ไม่พบหน้าเพจนี้</>
  return <div>404 ไม่พบหน้าเพจนี้</div>
}

export default NotFoundPage
