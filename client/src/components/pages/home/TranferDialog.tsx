import React, { useState } from 'react'
import { Dialog } from 'primereact/dialog'
import { PocketType, TranferPayload } from '../../../interfaces/pocketInterface'
import { InputNumber } from 'primereact/inputnumber'
import { Button } from 'primereact/button'
import usePocket from '../../../hooks/pocketHook'
import { AxiosError } from 'axios'
import { Toast } from 'primereact/toast'
import { Messages } from 'primereact/messages'
import { InputText } from 'primereact/inputtext'
import { User } from '../../../interfaces/userInterface'

const TranferDialog: React.FC<{
  onClose: (payload: { visible: boolean; amount?: number }) => void
  visible: boolean
}> = ({ onClose, visible }) => {
  const { toastRef, showErrorMessages, errorMessagesRef, user, checkUserTranfer } = usePocket()
  const [open, setOpen] = useState(false)
  const initForm: TranferPayload = {
    amount: null,
    bankAccountNo: '',
    type: PocketType.TRANFER
  }
  const [form, setForm] = useState<TranferPayload>(initForm)
  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      await checkUserTranfer({ amount: form.amount, bankAccountNo: form.bankAccountNo })
      setOpen(true)
    } catch (err) {
      const error = err as AxiosError<{ message: string }>
      showErrorMessages(error.response?.data?.message?.split(', '))
    }
  }

  const onHide = () => {
    onClose({ visible: false })
    setForm(initForm)
  }

  return (
    <>
      <Toast ref={toastRef} />
      <Dialog header='Tranfer' visible={visible} onHide={onHide} footer={<></>} style={{ maxWidth: '500px' }}>
        <form onSubmit={onSubmit}>
          <Messages ref={errorMessagesRef} />
          <span className='field'>
            <label htmlFor='amount'>หมายเลขบัญชี</label>
            <InputText
              className='w-full mb-2'
              value={form.bankAccountNo}
              maxLength={10}
              minLength={10}
              onChange={e => setForm({ ...form, bankAccountNo: e.target.value })}
            />
          </span>
          <span className='field'>
            <label htmlFor='amount'>จำนวนเงิน</label>
            <InputNumber
              className='w-full mb-2'
              value={form.amount}
              onChange={e => setForm({ ...form, amount: e.value })}
              maxFractionDigits={2}
              placeholder='0.00'
            />
          </span>
          <span className='field'>
            <Button label='ตรวจสอบ' />
          </span>
        </form>
      </Dialog>
      {user && <TranferValidate visible={open} onClose={() => setOpen(false)} user={user} amount={form.amount} onSuccess={onHide} />}
    </>
  )
}

export default TranferDialog

const TranferValidate: React.FC<{
  onClose: () => void
  onSuccess: () => void
  visible: boolean
  user: User
  amount: number | null
}> = ({ onClose, visible, user, amount, onSuccess }) => {
  const { transfer, showErrorMessages, errorMessagesRef } = usePocket()
  const tranferSubmit = async () => {
    try {
      await transfer({
        amount,
        bankAccountNo: user.bankAccountNo,
        type: PocketType.TRANFER
      })
      onSuccess()
      onClose()
    } catch (err) {
      const error = err as AxiosError<{ message: string }>
      showErrorMessages(error.response?.data?.message?.split(', '))
    }
  }
  return (
    <Dialog
      header='Validate'
      visible={visible}
      onHide={() => onClose()}
      footer={
        <div>
          <Button label='ยืนยันการโอน' onClick={tranferSubmit} />
        </div>
      }
      style={{ maxWidth: '390px' }}
    >
      <Messages ref={errorMessagesRef} />
      <div className='grid'>
        <div className='col-12'>
          <b>หมายเลขบัญชี: </b>
          <span>{user?.bankAccountNo} </span>
        </div>
        <div className='col-12'>
          <b>ชื่อผู้ใช้บัญชี: </b>
          <span>{user?.fullname} </span>
        </div>
        <div className='col-12'>
          <b>จำนวนเงิน: </b>
          <span>{amount} </span>
        </div>
      </div>
    </Dialog>
  )
}
