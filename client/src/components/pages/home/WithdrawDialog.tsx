import React, { useState } from 'react'
import { Dialog } from 'primereact/dialog'
import { WithdrawPayload, PocketType } from '../../../interfaces/pocketInterface'
import { InputNumber } from 'primereact/inputnumber'
import { Button } from 'primereact/button'
import usePocket from '../../../hooks/pocketHook'
import { AxiosError } from 'axios'
import { Toast } from 'primereact/toast'
import { Messages } from 'primereact/messages'

const WithdrawDialog: React.FC<{
  onClose: (payload: { visible: boolean; amount?: number }) => void
  visible: boolean
}> = ({ onClose, visible }) => {
  const { withdraw, toastRef, showErrorMessages, errorMessagesRef } = usePocket()
  const initForm: WithdrawPayload = {
    amount: null,
    type: PocketType.WITHDRAW
  }
  const [form, setForm] = useState<WithdrawPayload>(initForm)
  const initiComponents = () => {
    onClose({ visible: false })
    setForm(initForm)
  }
  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      await withdraw(form)
      initiComponents()
    } catch (err) {
      const error = err as AxiosError<{ message: string }>
      showErrorMessages(error.response?.data?.message?.split(', '))
    }
  }
  return (
    <>
      <Toast ref={toastRef} />
      <Dialog header='Withdraw' visible={visible} onHide={initiComponents} footer={<></>} style={{ maxWidth: '500px' }}>
        <form onSubmit={onSubmit}>
          <Messages ref={errorMessagesRef} />
          <span className='field'>
            <label htmlFor='amount'>จำนวนเงิน</label>
            <InputNumber className='w-full mb-2' value={form.amount} onChange={e => setForm({ ...form, amount: e.value })} />
          </span>
          <span className='field'>
            <Button label='ถอนเงิน' />
          </span>
        </form>
      </Dialog>
    </>
  )
}

export default WithdrawDialog
