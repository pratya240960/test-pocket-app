import React from 'react'
import { Pocket, PocketType } from '../../../interfaces/pocketInterface'
import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { Tag } from 'primereact/tag'

const TransectionTable: React.FC<{ transections: Pocket[] }> = ({ transections }) => {
  return (
    <>
      <DataTable
        value={transections}
        responsiveLayout='stack'
        breakpoint='2400px'
        className=' w-full'
        paginator
        rows={3}
        paginatorTemplate='CurrentPageReport PrevPageLink NextPageLink'
        currentPageReportTemplate='Showing {first} to {last} of {totalRecords}'
      >
        <Column field='amount' header='จำนวน' />
        <Column
          field='type'
          header='ประเภท'
          body={(row: Pocket) => {
            switch (row.type) {
              case PocketType.DEPOSIT:
                return <Tag severity='success' value='ฝาก'></Tag>
              case PocketType.WITHDRAW:
                return <Tag severity='warning' value='ถอน'></Tag>
              default:
                if (row.amount > 0) {
                  return <Tag severity='info' value='โอนเข้า'></Tag>
                } else {
                  return <Tag severity='danger' value='โอนออก'></Tag>
                }
            }
          }}
        />
        <Column
          field='createdAt'
          header='วันที่ดำเนินการ'
          body={(row: Pocket) => <span>{new Date(row.createdAt).toLocaleString('th-TH')}</span>}
        />
      </DataTable>
    </>
  )
}

export default TransectionTable
