import React, { useState } from 'react'
import { Dialog } from 'primereact/dialog'
import { PocketPayload, PocketType } from '../../../interfaces/pocketInterface'
import { InputNumber } from 'primereact/inputnumber'
import { Button } from 'primereact/button'
import usePocket from '../../../hooks/pocketHook'
import { AxiosError } from 'axios'
import { Toast } from 'primereact/toast'
import { Messages } from 'primereact/messages'

const DepositDialog: React.FC<{
  onClose: (payload: { visible: boolean; amount?: number }) => void
  visible: boolean
}> = ({ onClose, visible }) => {
  const { deposit, toastRef, showErrorMessages, errorMessagesRef } = usePocket()
  const initiForm: PocketPayload = {
    amount: null,
    type: PocketType.DEPOSIT
  }
  const [form, setForm] = useState<PocketPayload>(initiForm)
  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      await deposit(form)
      onClose({ visible: false })
      setForm(initiForm)
    } catch (err) {
      const error = err as AxiosError<{ message: string }>
      showErrorMessages(error.response?.data?.message?.split(', '))
    }
  }
  return (
    <>
      <Toast ref={toastRef} />
      <Dialog header='Deposit' visible={visible} onHide={() => onClose({ visible: false })} style={{ maxWidth: '500px' }} footer={<></>}>
        <form onSubmit={onSubmit}>
          <Messages ref={errorMessagesRef} />
          <span className='field'>
            <label htmlFor='amount'>จำนวนเงิน</label>
            <InputNumber
              className='w-full mb-2'
              value={form.amount}
              onChange={e => setForm({ ...form, amount: e.value })}
              maxFractionDigits={2}
              placeholder='0.00'
            />
          </span>
          <span className='field'>
            <Button label='ฝากเงิน' />
          </span>
        </form>
      </Dialog>
    </>
  )
}

export default DepositDialog
