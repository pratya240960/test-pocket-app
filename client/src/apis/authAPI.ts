import axios from 'axios'
import configs from '../configs'
import { LoginPayload } from '../interfaces/authInterface'
import { User, UserPayload } from '../interfaces/userInterface'
import instance from '../services/axiosService'

const basePath = 'auth'
export interface RefreshToken {
  status: number
  data: {
    message: string
    result: User
  }
}

const getProfile = () => instance.get<{ message: string; result: User }>(`${basePath}/profile`)
const signin = (payload: LoginPayload) => instance.post<{ message: string; result: User }>(`${basePath}/signin`, payload)
const signup = (payload: UserPayload) => instance.post<{ message: string; result: User }>(`${basePath}/signup`, payload)
const signout = () => instance.delete<{ message: string; result: User }>(`${basePath}/signout`)

export const refreshToken = (): Promise<RefreshToken> =>
  axios(`${configs.baseURL}/${basePath}/refresh-token`, {
    method: 'POST',
    withCredentials: true
  })
const authAPI = {
  getProfile,
  signin,
  signout,
  signup
}
export default authAPI
