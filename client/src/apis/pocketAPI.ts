import { CheckUserTranfer, Pocket, PocketPayload, TranferPayload, WithdrawPayload } from '../interfaces/pocketInterface'
import { User } from '../interfaces/userInterface'
import instance from '../services/axiosService'

const basePath = 'pockets'

const getBalance = () => instance.get<{ message: string; result: number }>(`${basePath}/balance`)
const getTransection = () => instance.get<{ message: string; result: Pocket[] }>(`${basePath}/transection`)
const deposit = (payload: PocketPayload) => instance.post<{ message: string; result: Pocket }>(`${basePath}/deposit`, payload)
const withdraw = (payload: WithdrawPayload) => instance.post<{ message: string; result: number }>(`${basePath}/withdraw`, payload)
const transfer = (payload: TranferPayload) => instance.post<{ message: string; result: number }>(`${basePath}/transfer`, payload)
const checkUserTranfer = (payload: CheckUserTranfer) =>
  instance.post<{ message: string; result: User }>(`${basePath}/user-tranfer`, payload)

const pocketAPI = {
  getBalance,
  getTransection,
  checkUserTranfer,
  deposit,
  withdraw,
  transfer
}

export default pocketAPI
