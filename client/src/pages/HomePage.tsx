import React, { useEffect, useState } from 'react'
import { Card } from 'primereact/card'
import usePocket from '../hooks/pocketHook'
import { Toast } from 'primereact/toast'
import { Button } from 'primereact/button'
import { TabView, TabPanel, TabViewTabChangeParams } from 'primereact/tabview'
import TransectionTable from '../components/pages/home/TransectionTable'
import DepositDialog from '../components/pages/home/DepositDialog'
import WithdrawDialog from '../components/pages/home/WithdrawDialog'
import TranferDialog from '../components/pages/home/TranferDialog'
import useAuth from '../hooks/authHook'

const HomePage: React.FC = () => {
  const [openDeposit, setOpenDeposit] = useState(false)
  const [openWithdraw, setOpenWithdraw] = useState(false)
  const [openTranfer, setOpenTranfer] = useState(false)
  const { toastRef, balance, getBalance, getTransection, transections } = usePocket()
  const { signout } = useAuth()
  const [tabIndex, setTabIndex] = useState(0)

  const initialPage = (index: number) => {
    if (index === 0) {
      getBalance()
    } else {
      getTransection()
    }
  }

  const onTabChange = (e: TabViewTabChangeParams) => {
    const index = e.index
    setTabIndex(index)
    initialPage(index)
  }

  const logout = () => {
    signout()
  }
  useEffect(() => {
    initialPage(tabIndex)
  }, [])

  return (
    <>
      <Toast ref={toastRef} />
      <div className='min-h-screen flex justify-content-center align-items-center'>
        <div className='p-card p-5'>
          <h3 className='p-card-title m-0'>กระเป๋าเงินของคุณ</h3>
          <div className=' p-card-content p-0'>
            <TabView onTabChange={onTabChange} activeIndex={tabIndex} className='py-0 my-0'>
              <TabPanel header='จำนวนเงินคงเหลือ'>
                <div className='flex justify-content-center'>
                  <h1>{balance.toLocaleString('en-US')}</h1>
                </div>
              </TabPanel>
              <TabPanel header='ประวัติการทำรายการ'>
                <div className=' overflow-y-scroll' style={{ maxHeight: '300px' }}></div>
                <TransectionTable transections={transections} />
              </TabPanel>
            </TabView>
          </div>
          <div className=' p-card-footer'>
            <div className='flex justify-content-center'>
              <span className='p-buttonset'>
                <Button label='Deposit' className=' p-button-success' onClick={() => setOpenDeposit(true)} />
                <Button label='Withdraw' className=' p-button-warning' onClick={() => setOpenWithdraw(true)} />
                <Button label='Tranfer' className=' p-button-info' onClick={() => setOpenTranfer(true)} />
              </span>
              <div className='flex justify-content-end'>
                <Button label='ออกจากระบบ' className=' p-button-text' onClick={logout} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <DepositDialog
        visible={openDeposit}
        onClose={({ visible }) => {
          setOpenDeposit(visible)
          initialPage(tabIndex)
        }}
      />
      <WithdrawDialog
        visible={openWithdraw}
        onClose={({ visible }) => {
          setOpenWithdraw(visible)
          initialPage(tabIndex)
        }}
      />
      <TranferDialog
        visible={openTranfer}
        onClose={({ visible }) => {
          setOpenTranfer(visible)
          initialPage(tabIndex)
        }}
      />
    </>
  )
}

export default HomePage
