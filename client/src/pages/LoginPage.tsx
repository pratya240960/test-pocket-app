import React, { useState } from 'react'
import { InputText } from 'primereact/inputtext'
import { Password } from 'primereact/password'
import { Button } from 'primereact/button'
import { AxiosError } from 'axios'
import useAlert from '../hooks/alertHook'
import { Messages } from 'primereact/messages'
import useAuth from '../hooks/authHook'
import { Card } from 'primereact/card'
import { Link } from 'react-router-dom'

const LoginPage: React.FC = () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const { errorMessagesRef, showErrorMessages } = useAlert()
  const { signin } = useAuth()
  const submit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      await signin({
        username,
        password
      })
    } catch (err) {
      const error = err as AxiosError<{ message: string }>
      showErrorMessages(error.response?.data?.message?.split(', '))
    }
  }
  return (
    <div className='flex justify-content-center py-5'>
      <Card style={{ width: '400px' }} className='shadow-2' title='เข้าสู่ระบบ'>
        <Messages ref={errorMessagesRef} />
        <form onSubmit={submit} className='w-full'>
          <div className='field'>
            <label htmlFor='username' className='block'>
              Username
            </label>
            <InputText
              id='username'
              value={username}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUsername(e.target.value)}
              className='w-full'
            />
          </div>
          <div className='field'>
            <label htmlFor='password' className='block'>
              Password
            </label>
            <InputText
              id='password'
              type={'password'}
              value={password}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)}
              className='w-full'
            />
          </div>
          <div className='field'>
            <Button label='เข้าสู่ระบบ' />
            <Link className=' p-button p-button-text' to={'/register'}>
              สมัครสมาชิก
            </Link>
          </div>
        </form>
      </Card>
    </div>
  )
}

export default LoginPage
