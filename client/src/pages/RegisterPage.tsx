import React, { useState } from 'react'
import { InputText } from 'primereact/inputtext'
import { Button } from 'primereact/button'
import { AxiosError } from 'axios'
import useAlert from '../hooks/alertHook'
import { Messages } from 'primereact/messages'
import useAuth from '../hooks/authHook'
import { Card } from 'primereact/card'
import { Link } from 'react-router-dom'

const RegisterPage: React.FC = () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [fullname, setFullname] = useState('')
  const [bankAccountNo, setBankAccountNo] = useState('')
  const { errorMessagesRef, showErrorMessages } = useAlert()
  const { signup } = useAuth()
  const submit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      await signup({
        fullname,
        bankAccountNo,
        username,
        password
      })
    } catch (err) {
      const error = err as AxiosError<{ message: string }>
      showErrorMessages(error.response?.data?.message?.split(', '))
    }
  }
  return (
    <div className='flex justify-content-center py-5'>
      <Card style={{ width: '400px' }} className='shadow-2' title='เข้าสู่ระบบ'>
        <Messages ref={errorMessagesRef} />
        <form onSubmit={submit} className='w-full'>
          <div className='field'>
            <label htmlFor='fullname' className='block'>
              Fullname
            </label>
            <InputText
              id='fullname'
              value={fullname}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setFullname(e.target.value)}
              className='w-full'
            />
          </div>
          <div className='field'>
            <label htmlFor='bankAccountNo' className='block'>
              Bank account number
            </label>
            <InputText
              id='bankAccountNo'
              value={bankAccountNo}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setBankAccountNo(e.target.value)}
              minLength={10}
              maxLength={10}
              className='w-full'
            />
          </div>
          <div className='field'>
            <label htmlFor='username' className='block'>
              Username
            </label>
            <InputText
              id='username'
              value={username}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUsername(e.target.value)}
              className='w-full'
            />
          </div>
          <div className='field'>
            <label htmlFor='password' className='block'>
              Password
            </label>
            <InputText
              id='password'
              type={'password'}
              value={password}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)}
              className='w-full'
            />
          </div>
          <div className='field'>
            <Button label='สมัคสมาชิก' />
            <Link className=' p-button p-button-text' to={'/login'}>
              เข้าสู่ระบบ
            </Link>
          </div>
        </form>
      </Card>
    </div>
  )
}

export default RegisterPage
