import { confirmPopup } from 'primereact/confirmpopup'
import { useDispatch } from 'react-redux'
import authAPI from '../apis/authAPI'
import { LoginPayload } from '../interfaces/authInterface'
import { User, UserPayload } from '../interfaces/userInterface'
import { setProfile } from '../stores/reducers/authReducer'
import useAlert from './alertHook'

const useAuth = () => {
  const { toastRef, errorMessagesRef, alertSuccessfull } = useAlert()
  const dispatch = useDispatch()
  const signup = async (payload: UserPayload) => {
    const { data } = await authAPI.signup(payload)
    alertSuccessfull()
    dispatch(setProfile(data.result))
  }
  const signin = async (payload: LoginPayload) => {
    const { data } = await authAPI.signin(payload)
    dispatch(setProfile(data.result))
  }
  const signout = async () => {
    await authAPI.signout()
    dispatch(setProfile(undefined))
  }
  return {
    errorMessagesRef,
    toastRef,
    signup,
    signin,
    signout
  }
}

export default useAuth
