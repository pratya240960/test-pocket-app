import { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import authAPI from '../apis/authAPI'
import { setProfile } from '../stores/reducers/authReducer'

const useLoadingWithProfile = () => {
  const [loading, setLoading] = useState(true)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadProfile = async () => {
      try {
        const { data } = await authAPI.getProfile()
        dispatch(setProfile(data.result))
        setLoading(false)
      } catch (err) {
        setLoading(false)
      }
    }
    loadProfile()
  }, [dispatch])

  return { loading }
}
export default useLoadingWithProfile
