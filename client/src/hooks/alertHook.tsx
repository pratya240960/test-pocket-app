import { Messages, MessagesMessage } from 'primereact/messages'
import { Toast } from 'primereact/toast'
import { useRef } from 'react'

const useAlert = () => {
  const toastRef = useRef<Toast>(null)
  const errorMessagesRef = useRef<Messages>(null)
  const alertSuccessfull = (summary = 'Confirmed', detail = 'ทำรายการสำเร็จ') => {
    toastRef.current?.show({ severity: 'success', summary, detail, life: 3000 })
  }
  const alertFiled = (summary = 'Error Message', detail = 'เกิดข้อผิดพลาด') => {
    toastRef.current?.show({ severity: 'error', summary, detail, life: 3000 })
  }
  const showErrorMessages = (message: string | string[] = 'เกิดข้อผิดพลาด') => {
    const errors: MessagesMessage[] = []
    if (Array.isArray(message)) {
      message.forEach(m => {
        errors.push({ severity: 'error', detail: m, sticky: true })
      })
    } else {
      errors.push({ severity: 'error', detail: message, sticky: true })
    }
    errorMessagesRef.current?.show(errors)
  }
  return {
    toastRef,
    errorMessagesRef,
    alertSuccessfull,
    alertFiled,
    showErrorMessages
  }
}
export default useAlert
