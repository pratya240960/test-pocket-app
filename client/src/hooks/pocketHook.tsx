import { useState } from 'react'
import pocketAPI from '../apis/pocketAPI'
import { CheckUserTranfer, Pocket, PocketPayload, TranferPayload, WithdrawPayload } from '../interfaces/pocketInterface'
import { User } from '../interfaces/userInterface'
import useAlert from './alertHook'

const usePocket = () => {
  const { toastRef, alertSuccessfull, errorMessagesRef, showErrorMessages } = useAlert()
  const [transections, setTransections] = useState<Pocket[]>([])
  const [balance, setBalance] = useState<number>(0)
  const [user, setUser] = useState<User | undefined>(undefined)
  const getBalance = async () => {
    const {
      data: { result }
    } = await pocketAPI.getBalance()
    setBalance(result)
  }
  const getTransection = async () => {
    const {
      data: { result }
    } = await pocketAPI.getTransection()
    setTransections(result)
  }

  const deposit = async (payload: PocketPayload) => {
    await pocketAPI.deposit(payload)
    alertSuccessfull()
  }

  const transfer = async (payload: TranferPayload) => {
    await pocketAPI.transfer(payload)
    alertSuccessfull()
  }

  const withdraw = async (payload: WithdrawPayload) => {
    await pocketAPI.withdraw(payload)
    alertSuccessfull()
  }

  const checkUserTranfer = async (payload: CheckUserTranfer) => {
    const {
      data: { result }
    } = await pocketAPI.checkUserTranfer(payload)
    setUser(result)
  }
  return {
    toastRef,
    transections,
    balance,
    getBalance,
    getTransection,
    deposit,
    transfer,
    withdraw,
    errorMessagesRef,
    showErrorMessages,
    setBalance,
    checkUserTranfer,
    user
  }
}
export default usePocket
