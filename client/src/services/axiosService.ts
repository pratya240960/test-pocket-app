import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import { refreshToken } from '../apis/authAPI'
import configs from '../configs'

export interface AxiosRequestConfigWithRetry extends AxiosRequestConfig {
  retry?: boolean
}

const initialConfigs: AxiosRequestConfig = {
  baseURL: configs.baseURL,
  withCredentials: true,
  headers: {
    'Content-type': 'application/json',
    Accept: 'application/json'
  }
}

const onRequest = (config: AxiosRequestConfig): AxiosRequestConfig => {
  return config
}

const onRequestError = (error: AxiosError): Promise<AxiosError> => {
  return Promise.reject(error)
}

const onResponse = (response: AxiosResponse): AxiosResponse => {
  return response
}

const onResponseError = async (error: AxiosError) => {
  console.error(`[response error] [${JSON.stringify(error.message)}]`)
  const originalRequest = error?.config as AxiosRequestConfigWithRetry
  if (error?.response?.status === 401 && !originalRequest.retry) {
    originalRequest.retry = true
    const response = await refreshToken()
    if (response.status === 200) {
      if (originalRequest) {
        return axios(originalRequest)
      }
    }
    window.location.reload()
  }
  return Promise.reject(error)
}

function setupInterceptors(axiosInstance: AxiosInstance): AxiosInstance {
  axiosInstance.interceptors.request.use(onRequest, onRequestError)
  axiosInstance.interceptors.response.use(onResponse, onResponseError)
  return axiosInstance
}
const instance = setupInterceptors(axios.create(initialConfigs))
export default instance
