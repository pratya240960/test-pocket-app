export interface User {
  id: number
  fullname: string
  bankAccountNo: string
  username: string
  password: string
}
export interface UserPayload {
  fullname: string
  bankAccountNo: string
  username: string
  password: string
}
