import { User } from './userInterface'
export enum PocketType {
  DEPOSIT = 'DEPOSIT',
  WITHDRAW = 'WITHDRAW',
  TRANFER = 'TRANFER'
}
export interface Pocket {
  id: number
  amount: number
  user: User
  userId: number
  createdAt: Date
  type: PocketType
}
export interface PocketPayload {
  amount: number | null
  type: PocketType.DEPOSIT
}
export interface WithdrawPayload {
  amount: number | null
  type: PocketType.WITHDRAW
}
export interface TranferPayload {
  amount: number | null
  bankAccountNo: string
  type: PocketType.TRANFER
}
export interface CheckUserTranfer {
  amount: number | null
  bankAccountNo: string
}
