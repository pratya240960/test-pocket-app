import { User } from './userInterface'

export interface ResponseInterface {
  message: string
}
export interface ResponseWithUser extends ResponseInterface {
  result: User
}
