# Backend server start

```
cd server
```

## Step 1. How to install

```
npm install
```

## Step 2. How to run docker

```shell
docker compose up -d
```

## Step 3. How to run migration database by prisma

- Prisma doc: https://www.prisma.io/

```shell
npm run prisma:dev
```

## Step 4. How to run node server

```shell
npm run dev
```

# Frontend client start

```
cd client
```

## Step 1. How to install

```
npm install
```

## Step 2. How to run react app

```shell
npm run start
```
