import { Router } from 'express'
import PocketController from '../controllers/pocket.controller'
import { CreatePocketDto, TransferDto, WithdrawDto } from '../dtos/pocket.dto'
import { Routes } from '../interfaces/route.interface'
import { authAnyMiddleware } from '../middlewares/auth.middleware'
import validatorMiddleware from '../middlewares/validator.middleware'
import { CheckUserTranfer } from '../dtos/pocket.dto'
export default class PocketRoute implements Routes {
   public readonly router: Router
   public readonly path = '/pockets'
   private readonly pocketCtlr: PocketController
   constructor() {
      this.pocketCtlr = new PocketController()
      this.router = Router()
      this.initGlobalMiddleware()
      this.initRoutes()
   }

   private initRoutes = () => {
      this.router.route(`${this.path}/balance`).get(this.pocketCtlr.getBalance)
      this.router.route(`${this.path}/transection`).get(this.pocketCtlr.getTransection)
      this.router.route(`${this.path}/deposit`).post([validatorMiddleware(CreatePocketDto)], this.pocketCtlr.deposit)
      this.router.route(`${this.path}/withdraw`).post([validatorMiddleware(WithdrawDto)], this.pocketCtlr.withdraw)
      this.router.route(`${this.path}/transfer`).post([validatorMiddleware(TransferDto)], this.pocketCtlr.transfer)
      this.router.route(`${this.path}/user-tranfer`).post([validatorMiddleware(CheckUserTranfer)], this.pocketCtlr.checkUserTranfer)
   }

   private initGlobalMiddleware = () => {
      this.router.use(authAnyMiddleware)
   }
}
