import { Router } from 'express'
import AuthController from '../controllers/auth.controller'
import { SigninDto } from '../dtos/auth.dto'
import { CreateUserDto } from '../dtos/user.dto'
import { Routes } from '../interfaces/route.interface'
import { authAnyMiddleware, refreshTokenMiddleware } from '../middlewares/auth.middleware'
import validatorMiddleware from '../middlewares/validator.middleware'
export default class AuthRoute implements Routes {
   public readonly router: Router
   public readonly path = '/auth'
   private readonly authCtlr: AuthController
   constructor() {
      this.router = Router()
      this.authCtlr = new AuthController()
      this.initRoutes()
   }

   private initRoutes = () => {
      this.router.get(`${this.path}/profile`, authAnyMiddleware, this.authCtlr.getProfile)
      this.router.post(`${this.path}/refresh-token`, refreshTokenMiddleware, this.authCtlr.refreshToken)
      this.router.post(`${this.path}/signin`, [validatorMiddleware(SigninDto, 'body')], this.authCtlr.signin)
      this.router.post(`${this.path}/signup`, [validatorMiddleware(CreateUserDto, 'body')], this.authCtlr.signup)
      this.router.delete(`${this.path}/signout`, authAnyMiddleware, this.authCtlr.signout)
   }
}
