import winston from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'
import config from '../config'

const levels = {
   error: 0,
   warn: 1,
   info: 2,
   http: 3,
   debug: 4,
}

const level = () => {
   return config.isDevelopment ? 'debug' : 'info'
}

const colors = {
   error: 'red',
   warn: 'yellow',
   info: 'green',
   http: 'magenta',
   debug: 'white',
}

winston.addColors(colors)

const format = winston.format.combine(
   winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
   winston.format.colorize({ all: true }),
   winston.format.printf(info => `[App] - ${info.timestamp} Log: ${info.message}`),
)

const transports = [
   new winston.transports.Console(),
   new DailyRotateFile({
      dirname: 'logs',
      filename: 'application-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      level: level(),
   }),
]

const Logger = winston.createLogger({
   level: level(),
   levels,
   format,
   transports,
})

export default Logger
