import { NextFunction, Response } from 'express'
import jwt from 'jsonwebtoken'
import config from '../config'
import { PayloadJWT } from '../interfaces/jwt.interface'
import { RequestWithUser } from '../interfaces/http.interface'
import HttpException, { HttpStatus } from '../exceptions'
import prisma from '../services/prisma.service'

const verifyJwt = async (req: RequestWithUser, ___: Response, next: NextFunction, jwtAuth: string | null, secretKey: string) => {
   if (jwtAuth) {
      const verificationResponse = jwt.verify(jwtAuth, secretKey) as PayloadJWT
      const userId = +verificationResponse.sub
      const findUser = await prisma.user.findFirst({ where: { id: userId } })
      if (findUser) {
         req.user = findUser
         next()
      } else {
         next(new HttpException(HttpStatus.UNAUTHORIZED, 'Wrong authentication token'))
      }
   } else {
      next(new HttpException(HttpStatus.UNAUTHORIZED, 'Authentication token missing'))
   }
}

const jwtMiddleware = async (req: RequestWithUser, res: Response, next: NextFunction) => {
   try {
      const jwtAuth = req.cookies['Authentication'] || req.header('Authentication')?.split('Bearer ')[1] || null
      const secretKey: string = config.jwtAccessTokenSecret
      await verifyJwt(req, res, next, jwtAuth, secretKey)
   } catch (error) {
      next(new HttpException(HttpStatus.UNAUTHORIZED, 'Wrong authentication token'))
   }
}

export const authAnyMiddleware = async (req: RequestWithUser, res: Response, next: NextFunction) => jwtMiddleware(req, res, next)

export const refreshTokenMiddleware = async (req: RequestWithUser, res: Response, next: NextFunction) => {
   try {
      const jwtAuth = req.cookies['RefreshToken'] || req.header('RefreshToken') || null
      const secretKey: string = config.jwtRefreshTokenSecret
      await verifyJwt(req, res, next, jwtAuth, secretKey)
   } catch (error) {
      next(new HttpException(HttpStatus.UNAUTHORIZED, 'Wrong authentication refresh token'))
   }
}
