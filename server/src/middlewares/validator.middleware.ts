import { ClassConstructor, plainToClass } from 'class-transformer'
import { validate, ValidationError } from 'class-validator'
import { RequestHandler } from 'express'
import HttpException from '../exceptions'

const validatorMiddleware = (
   // eslint-disable-next-line @typescript-eslint/no-explicit-any
   type: ClassConstructor<any>,
   value: string | 'body' | 'query' | 'params' = 'body',
   skipMissingProperties = false,
   whitelist = true,
   forbidNonWhitelisted = true,
): RequestHandler => {
   return (req, __, next) => {
      const dtoObj = plainToClass(type, req[value])
      validate(dtoObj, {
         skipMissingProperties,
         whitelist,
         forbidNonWhitelisted,
         skipUndefinedProperties: skipMissingProperties,
         skipNullProperties: skipMissingProperties,
      }).then((errors: ValidationError[]) => {
         if (errors.length > 0) {
            const message = errors.map((error: ValidationError) => Object.values(error.constraints).join(', ')).join(', ')
            next(new HttpException(400, message))
         } else {
            next()
         }
      })
   }
}

export default validatorMiddleware
