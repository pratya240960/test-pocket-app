import morgan, { StreamOptions } from 'morgan'
import config from '../config'
import Logger from '../utilities/logger.utils'

const stream: StreamOptions = {
   write: (message: string) => Logger.http(message),
}
const skip = () => config.isProduction
const morganMiddleware = morgan('[:method] :url :status :res[content-length] - :response-time ms', { stream, skip })
export default morganMiddleware
