import { Prisma } from '@prisma/client'
import { NextFunction, Request, Response } from 'express'
import HttpException, { HttpStatus } from '../exceptions'
import Logger from '../utilities/logger.utils'
const errorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
   console.error(error)
   try {
      let status: HttpStatus = error.status || HttpStatus.INTERNAL_SERVER_ERROR
      let message: string = error.message || 'Internal Server Error'
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
         if (error.code === 'P2002') {
            status = HttpStatus.CONFLICT
            const field = error.meta['target'] || ''
            message = `The value "${req.body[`${field}`] || ''}" of the ${field} fields has been used!!`
         }
      }
      Logger.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`)
      res.status(status).json({ message })
   } catch (error) {
      next(error)
   }
}

export default errorMiddleware
