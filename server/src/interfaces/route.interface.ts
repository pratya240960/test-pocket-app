import { Router } from 'express'

export interface Routes {
   prefixPath?: string
   router: Router
}
