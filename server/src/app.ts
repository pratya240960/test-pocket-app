import cors from 'cors'
import config from './config'
import morganMiddleware from './middlewares/logger.middleware'
import Logger from './utilities/logger.utils'
import express, { Application as ExApplication } from 'express'
import cookieParser from 'cookie-parser'
import 'dotenv/config'
import { Routes } from './interfaces/route.interface'
import errorMiddleware from './middlewares/error.middleware'
export default class Application {
   constructor(private readonly routes?: Routes[]) {
      this.app = express()
      this.initMiddlewares()
      this.initRoutes(this.routes)
      this.initErrorHandling()
   }
   private readonly app: ExApplication
   get appServer(): ExApplication {
      return this.app
   }

   public init = (port = 3000) => {
      return this.app.listen(port, () => {
         Logger.info(`==========================================`)
         Logger.info(`------------ ENV: ${config.appNode} ------------`)
         Logger.info(`--- 🚀 App listening on the port ${port} ----`)
         Logger.info(`==========================================`)
      })
   }

   private initRoutes = (routes?: Routes[]) => {
      this.app.get('/', (_, res) => {
         res.json({ message: 'Hello World!' })
      })
      routes?.forEach(route => {
         this.app.use('/api', route.router)
      })
   }

   private initMiddlewares = () => {
      this.app.use(morganMiddleware)
      this.app.use(cors({ origin: true, credentials: true }))
      this.app.use(express.json())
      this.app.use(express.urlencoded({ extended: true }))
      this.app.use(cookieParser())
   }

   private initErrorHandling = () => {
      this.app.use(errorMiddleware)
   }
}
