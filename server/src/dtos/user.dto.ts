import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator'

export class CreateUserDto {
   @IsNotEmpty()
   @IsString()
   fullname: string
   @IsNotEmpty()
   @IsString()
   @MinLength(10)
   @MaxLength(10)
   bankAccountNo: string
   @IsNotEmpty()
   @IsString()
   username: string
   @IsNotEmpty()
   @IsString()
   password: string
}
