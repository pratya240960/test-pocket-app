import { PocketType } from '@prisma/client'
import { IsEnum, IsNotEmpty, IsNumber, IsString, Matches, MaxLength, Min, MinLength } from 'class-validator'

export class CreatePocketDto {
   @IsNotEmpty()
   @IsNumber()
   @Min(0.01)
   amount: number

   userId: number

   @IsNotEmpty()
   @IsEnum(PocketType)
   @Matches(PocketType.DEPOSIT)
   type: PocketType
}

export class WithdrawDto {
   @IsNotEmpty()
   @IsNumber()
   @Min(1)
   amount: number

   @IsNotEmpty()
   @IsEnum(PocketType)
   @Matches(PocketType.WITHDRAW)
   type: PocketType
}

export class TransferDto {
   @IsNotEmpty()
   @IsNumber()
   @Min(0.01)
   amount: number

   @IsNotEmpty()
   @IsString()
   @MinLength(10)
   @MaxLength(10)
   bankAccountNo: string

   @IsNotEmpty()
   @IsEnum(PocketType)
   @Matches(PocketType.TRANFER)
   type: PocketType
}

export class CheckUserTranfer {
   @IsNotEmpty()
   @IsNumber()
   @Min(0.01)
   amount: number
   @IsNotEmpty()
   @IsString()
   @MinLength(10)
   @MaxLength(10)
   bankAccountNo: string
}
