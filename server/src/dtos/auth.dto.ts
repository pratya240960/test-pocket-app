import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class SigninDto {
   @IsNotEmpty()
   @IsString()
   username: string

   @IsNotEmpty()
   @IsString()
   password: string

   @IsOptional()
   @IsBoolean()
   isRemember?: boolean
}
