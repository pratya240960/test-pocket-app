import Application from './app'
import { Socket } from 'net'
import AuthRoute from './routes/auth.route'
import config from './config'
import PocketRoute from './routes/pocket.route'

const routes = [new AuthRoute(), new PocketRoute()]
const app = new Application(routes)
let connections: Socket[] = []
const server = app.init(+config.appPort)
server.on('connection', conn => {
   connections.push(conn)
   conn.on('close', () => (connections = connections.filter(curr => curr !== conn)))
})

const shutdown = () => {
   server.close(() => {
      console.log('App closed out remaining connections')
      process.exit(0)
   })

   setTimeout(() => {
      console.log(`App could not close remaining connections: ${connections.length}`)
      process.exit(1)
   }, 10000)

   connections.forEach(curr => curr.end())
   setTimeout(() => {
      connections.forEach(curr => curr.destroy())
   }, 5000)
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)
