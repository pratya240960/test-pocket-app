import { Request, Response, NextFunction } from 'express'
import { CheckUserTranfer, CreatePocketDto, TransferDto, WithdrawDto } from '../dtos/pocket.dto'
import HttpException, { HttpStatus } from '../exceptions'
import { RequestWithUser } from '../interfaces/http.interface'
import PocketService from '../services/pocket.service'
import UserService from '../services/user.service'

export default class PocketController {
   private pocketSrv: PocketService
   private userSrv: UserService
   constructor() {
      this.pocketSrv = new PocketService()
      this.userSrv = new UserService()
   }

   public getTransection = async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
         const balance = await this.pocketSrv.getPockets({ userId: +req.user.id })
         res.json({ message: 'success', result: balance })
      } catch (error) {
         next(error)
      }
   }

   public getBalance = async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
         const balance = await this.pocketSrv.getBalance(+req.user.id)
         res.json({ message: 'success', result: balance || 0 })
      } catch (error) {
         next(error)
      }
   }

   public getPocketList = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const pockets = await this.pocketSrv.getPockets()
         res.json({ message: 'success', result: pockets })
      } catch (error) {
         next(error)
      }
   }

   public getPocket = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const pocket = await this.pocketSrv.getPocket({ id: +req.params.id })
         res.json({ message: 'success', result: pocket })
      } catch (error) {
         next(error)
      }
   }

   public deposit = async (req: RequestWithUser, res: Response, next: NextFunction) => {
      const reqBody = req.body as CreatePocketDto
      reqBody.userId = +req.user.id
      try {
         const pocket = await this.pocketSrv.createPocket(reqBody)
         res.json({ message: 'success', result: pocket })
      } catch (error) {
         next(error)
      }
   }

   public withdraw = async (req: RequestWithUser, res: Response, next: NextFunction) => {
      const reqBody = req.body as WithdrawDto
      try {
         const amount = await this.pocketSrv.getBalance(+req.user.id)
         if (amount < reqBody.amount) return next(new HttpException(HttpStatus.BAD_REQUEST, 'จำนวนเงินของคุณไม่เพียงพอ'))
         await this.pocketSrv.createPocket({
            amount: 0 - reqBody.amount,
            userId: +req.user.id,
            type: reqBody.type,
         })
         res.json({ message: 'success', result: amount - reqBody.amount })
      } catch (error) {
         next(error)
      }
   }

   public checkUserTranfer = async (req: RequestWithUser, res: Response, next: NextFunction) => {
      const reqBody = req.body as CheckUserTranfer
      try {
         const recipient = await this.pocketSrv.checkUserTranfer(reqBody, req.user)
         res.json({
            message: 'success',
            result: recipient,
         })
      } catch (error) {
         next(error)
      }
   }

   public transfer = async (req: RequestWithUser, res: Response, next: NextFunction) => {
      const reqBody = req.body as TransferDto
      try {
         const recipient = await this.pocketSrv.checkUserTranfer(reqBody, req.user)
         await this.pocketSrv.tranfer({
            recipientId: recipient.id,
            actorId: +req.user.id,
            amount: reqBody.amount,
         })
         res.json({ message: 'success', result: reqBody.amount })
      } catch (error) {
         next(error)
      }
   }

   public updatePocket = async (req: Request, res: Response, next: NextFunction) => {
      const reqBody = req.body as CreatePocketDto
      try {
         const pocket = await this.pocketSrv.updatePocket(+req.params.id, reqBody)
         res.json({ message: 'success', result: pocket })
      } catch (error) {
         next(error)
      }
   }

   public deletePocket = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const pocket = await this.pocketSrv.deletePocket(+req.params.id)
         res.json({ message: 'success', result: pocket })
      } catch (error) {
         next(error)
      }
   }
}
