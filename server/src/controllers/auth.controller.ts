import { User } from '@prisma/client'
import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import config from '../config'
import { SigninDto } from '../dtos/auth.dto'
import { CreateUserDto } from '../dtos/user.dto'
import { RequestWithUser } from '../interfaces/http.interface'
import AuthService from '../services/auth.service'
import UserService from '../services/user.service'

export default class AuthController {
   private readonly authSrv: AuthService
   private readonly userSrv: UserService
   private readonly jwtSrv = jwt
   constructor() {
      this.authSrv = new AuthService()
      this.userSrv = new UserService()
   }

   private signTokenCookies = (res: Response, user: User, isRemember = false) => {
      const accessToken = this.jwtSrv.sign(
         {
            sub: user.id,
         },
         config.jwtAccessTokenSecret,
         { expiresIn: '15m' },
      )
      const refreshToken = this.jwtSrv.sign(
         {
            sub: user.id,
         },
         config.jwtRefreshTokenSecret,
         {
            expiresIn: isRemember ? '7d' : '12h',
         },
      )
      const maxAgeRefresh = isRemember ? 1000 * 60 * 60 * 24 * 7 : 1000 * 60 * 60 * 12
      const maxAge = 1000 * 60 * 15
      res.cookie('Authentication', accessToken, {
         httpOnly: true,
         sameSite: 'lax',
         maxAge,
         expires: new Date(maxAge),
         secure: config.isProduction,
      })
      res.cookie('RefreshToken', refreshToken, {
         httpOnly: true,
         sameSite: 'lax',
         maxAge: maxAgeRefresh,
         expires: new Date(maxAgeRefresh),
         secure: config.isProduction,
      })

      isRemember &&
         res.cookie('IsRemember', isRemember, {
            httpOnly: true,
            sameSite: 'lax',
            maxAge: maxAgeRefresh,
            expires: new Date(maxAgeRefresh),
            secure: config.isProduction,
         })
      res.json({ message: 'success', result: user })
   }

   public getProfile = async (req: RequestWithUser, res: Response) => {
      res.json({ message: 'success', result: req.user })
   }

   public refreshToken = async (req: RequestWithUser, res: Response) => {
      this.signTokenCookies(res, req.user, req.cookies['IsRemember'])
   }

   public signin = async (req: Request, res: Response, next: NextFunction) => {
      const reqBody = req.body as SigninDto
      try {
         const user = await this.authSrv.signin(reqBody)
         this.signTokenCookies(res, user, reqBody.isRemember)
      } catch (error) {
         next(error)
      }
   }
   public signup = async (req: Request, res: Response, next: NextFunction) => {
      const reqBody = req.body as CreateUserDto
      try {
         const user = await this.userSrv.createUser(reqBody)
         this.signTokenCookies(res, user)
      } catch (error) {
         next(error)
      }
   }

   public signout = async (_: Request, res: Response) => {
      res.clearCookie('Authentication', { maxAge: 0 })
      res.clearCookie('RefreshToken', { maxAge: 0 })
      res.clearCookie('IsRemember', { maxAge: 0 })
      res.json({ message: 'success' })
   }
}
