import { config } from 'dotenv'

config({ path: `.env.${process.env.NODE_ENV}` })

export default {
   // application configurations
   appNode: process.env.NODE_ENV,
   appName: process.env.APP_NAME || '',
   appPort: process.env.APP_PORT || 3000,

   // application mode
   isStaging: process.env.NODE_ENV === 'staging',
   isProduction: process.env.NODE_ENV === 'production',
   isDevelopment: process.env.NODE_ENV === 'development',
   isTest: process.env.NODE_ENV === 'test',

   // json web token secret
   jwtAccessTokenSecret: process.env.JWT_ACCESS_TOKEN_SECRET,
   jwtRefreshTokenSecret: process.env.JWT_REFRESH_TOKEN_SECRET,

   // database configs
   dbURL: process.env.DATABASE_URL,
}
