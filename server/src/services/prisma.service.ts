import { PrismaClient } from '@prisma/client'
import config from '../config'
import HttpException, { HttpStatus } from '../exceptions'

export class PrismaService extends PrismaClient {
   constructor() {
      super({
         datasources: {
            db: {
               url: config.dbURL,
            },
         },
         rejectOnNotFound: {
            findUnique: {
               User: () => new HttpException(HttpStatus.NOT_FOUND, 'No User found'),
               Pocket: () => new HttpException(HttpStatus.NOT_FOUND, 'No Pocket found'),
            },
         },
      })
   }

   cleanDb() {
      return this.$transaction([this.user.deleteMany()])
   }
}

let prisma: PrismaService
if (config.isProduction) {
   prisma = new PrismaService()
} else {
   if (!global.prisma) {
      global.prisma = new PrismaService()
   }
   prisma = global.prisma
}

export default prisma
