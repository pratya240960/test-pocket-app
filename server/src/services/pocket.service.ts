import { Prisma, Pocket, PocketType, User } from '@prisma/client'
import { DebugLog } from '../decorators/logger.decorator'
import { CheckUserTranfer, CreatePocketDto } from '../dtos/pocket.dto'
import HttpException, { HttpStatus } from '../exceptions'
import prisma, { PrismaService } from './prisma.service'

export default class PocketService {
   private readonly context: PrismaService
   constructor() {
      this.context = prisma
   }

   @DebugLog({ name: 'PocketService' })
   public async getBalance(userId: number): Promise<number> {
      return (
         await this.context.pocket.aggregate({
            _sum: {
               amount: true,
            },
            where: {
               userId,
            },
         })
      )._sum.amount
   }

   @DebugLog({ name: 'PocketService' })
   public getPockets(payload?: Prisma.PocketWhereInput): Promise<Pocket[]> {
      return this.context.pocket.findMany({
         where: payload,
         orderBy: {
            createdAt: 'desc',
         },
      })
   }

   @DebugLog({ name: 'PocketService' })
   public getPocket(payload?: Prisma.PocketWhereInput): Promise<Pocket> {
      return this.context.pocket.findFirst({ where: payload })
   }

   @DebugLog({ name: 'PocketService' })
   public getPocketDetail(id: number): Promise<Pocket> {
      return this.context.pocket.findUnique({ where: { id } })
   }

   @DebugLog({ name: 'PocketService' })
   public createPocket(payload: CreatePocketDto): Promise<Pocket> {
      return this.context.pocket.create({ data: payload })
   }

   @DebugLog({ name: 'PocketService' })
   public async tranfer(payload: { recipientId: number; actorId: number; amount: number }) {
      return this.context.$transaction([
         this.context.pocket.create({
            data: {
               userId: payload.recipientId,
               amount: payload.amount,
               type: PocketType.TRANFER,
            },
         }),
         this.context.pocket.create({
            data: {
               userId: payload.actorId,
               amount: 0 - payload.amount,
               type: PocketType.TRANFER,
            },
         }),
      ])
   }

   @DebugLog({ name: 'PocketService' })
   public async updatePocket(id: number, payload: CreatePocketDto): Promise<Pocket> {
      await this.getPocketDetail(id)
      return this.context.pocket.update({ where: { id }, data: payload })
   }

   @DebugLog({ name: 'PocketService' })
   public async deletePocket(id: number): Promise<Pocket> {
      await this.getPocketDetail(id)
      return this.context.pocket.delete({ where: { id } })
   }

   @DebugLog({ name: 'PocketService' })
   public async checkUserTranfer(payload: CheckUserTranfer, user: User): Promise<User> {
      if (payload.bankAccountNo === user.bankAccountNo || !payload.bankAccountNo)
         throw new HttpException(HttpStatus.NOT_FOUND, 'ไม่สามารถทำรายการได้')
      const amount = await this.getBalance(+user.id)
      if (amount < payload.amount) throw new HttpException(HttpStatus.BAD_REQUEST, 'จำนวนเงินของคุณไม่เพียงพอ')
      const result = await this.context.user.findFirst({ where: { bankAccountNo: payload.bankAccountNo } })
      if (!result?.id) throw new HttpException(HttpStatus.NOT_FOUND, 'หมายเลขบัญชีไม่ถูกต้อง')
      return result
   }
}
