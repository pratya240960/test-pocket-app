import { User } from '@prisma/client'
import { DebugLog } from '../decorators/logger.decorator'
import { SigninDto } from '../dtos/auth.dto'
import HttpException, { HttpStatus } from '../exceptions'
import prisma, { PrismaService } from './prisma.service'
import * as bcrypt from 'bcrypt'

export default class AuthService {
   private readonly context: PrismaService
   constructor() {
      this.context = prisma
   }

   @DebugLog({ name: 'AuthService' })
   public async signin(reqBody: SigninDto): Promise<User> {
      const validateUsername = await this.context.user.findFirst({
         where: {
            username: reqBody.username,
         },
      })
      if (!validateUsername) {
         throw new HttpException(HttpStatus.NOT_FOUND, 'Username or password incorec!')
      }
      const validatePassword = this.comparePassword(reqBody.password, validateUsername.password)
      if (!validatePassword) {
         throw new HttpException(HttpStatus.NOT_FOUND, 'Username or password incorec!')
      }
      delete validateUsername.password
      return validateUsername
   }

   @DebugLog({ name: 'AuthService' })
   public comparePassword(password: string, comparePassword: string) {
      return bcrypt.compareSync(password, comparePassword)
   }

   @DebugLog({ name: 'AuthService' })
   public hashPassword(password: string) {
      return bcrypt.hashSync(password, 10)
   }
}
