import { Prisma, User } from '@prisma/client'
import { DebugLog } from '../decorators/logger.decorator'
import prisma, { PrismaService } from './prisma.service'
import { CreateUserDto } from '../dtos/user.dto'
import AuthService from './auth.service'

export default class UserService {
   private readonly context: PrismaService
   private readonly authSrv: AuthService
   constructor() {
      this.context = prisma
      this.authSrv = new AuthService()
   }

   @DebugLog({ name: 'UserService' })
   public getUsers(payload?: Prisma.UserWhereInput): Promise<User[]> {
      return this.context.user.findMany({ where: payload })
   }

   @DebugLog({ name: 'UserService' })
   public getUser(payload?: Prisma.UserWhereInput): Promise<User> {
      return this.context.user.findFirst({ where: payload })
   }

   @DebugLog({ name: 'UserService' })
   public getUserDetail(id: number): Promise<User> {
      return this.context.user.findUnique({ where: { id } })
   }

   @DebugLog({ name: 'UserService' })
   createUser(payload: CreateUserDto) {
      payload.password = this.authSrv.hashPassword(payload.password)
      return this.context.user.create({ data: payload })
   }
}
