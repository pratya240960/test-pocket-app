/* eslint-disable @typescript-eslint/no-explicit-any */
import Logger from '../utilities/logger.utils'

interface LoggerParams {
   name: string
   inputs?: boolean
   outputs?: boolean
}

export const DebugLog = (params: LoggerParams) => {
   const options: Required<LoggerParams> = {
      name: params.name,
      inputs: params?.inputs === undefined ? true : params.inputs,
      outputs: params?.outputs === undefined ? true : params.outputs,
   }
   return function (target: object, propertyKey: string, descriptor: PropertyDescriptor) {
      const originalMethod = descriptor.value
      descriptor.value = function (...args: any[]) {
         if (options.inputs) {
            Logger.info(`[${params.name}] method: ${propertyKey} - inputs: ${JSON.stringify(args)}`)
         }
         const data = originalMethod.apply(this, args)
         if (options.outputs) {
            if (data.then) {
               data
                  .then((result: any) => Logger.info(`[${params.name}] method: ${propertyKey} - outputs: ${JSON.stringify(result)}`))
                  .catch((error: any) => new Error(error))
            } else {
               Logger.info(`[${params.name}] method: ${propertyKey} - outputs: ${data}`)
            }
         }
         return data
      }
   }
}
