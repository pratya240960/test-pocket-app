/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
   moduleFileExtensions: ['js', 'json', 'ts'],
   rootDir: '__test__',
   testRegex: '.*\\app.(spec|test)\\.ts$',
   transform: {
      '^.+\\.(t|j)s$': 'ts-jest',
   },
   collectCoverageFrom: ['**/*.(t|j)s'],
   coverageDirectory: '../coverage',
   testEnvironment: 'node',
}
