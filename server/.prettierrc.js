module.exports = {
   bracketSpacing: true,
   singleQuote: true,
   trailingComma: 'all',
   arrowParens: 'avoid',
   semi: false,
   tabWidth: 3,
   printWidth: 150,
   // Override any other rules you want
}
