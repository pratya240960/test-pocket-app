/*
  Warnings:

  - Added the required column `type` to the `Pocket` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "PocketType" AS ENUM ('DEPOSIT', 'WITHDRAW', 'TRANFER');

-- AlterTable
ALTER TABLE "Pocket" ADD COLUMN     "type" "PocketType" NOT NULL;
