/*
  Warnings:

  - You are about to drop the column `aomunt` on the `Pocket` table. All the data in the column will be lost.
  - Added the required column `amount` to the `Pocket` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Pocket" DROP COLUMN "aomunt",
ADD COLUMN     "amount" INTEGER NOT NULL;
